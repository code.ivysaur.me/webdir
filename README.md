# webdir

![](https://img.shields.io/badge/written%20in-golang-blue)

A web-based directory viewer.

Host a read-only view of a directory, allowing users to browse the content.

## Features

- Windows Explorer-style theme
- Browse subdirectories and download files
- Thumbnail preview for image files and video files (with `ffmpeg` installed)

The thumbnailer code can also be compiled as a standalone utility (`mkthumb`). This binary is not required to use `webdir`, but is shipped as an extra utility for convenience and testing.

## Usage


```

Usage of ./webdir:
  -Basedir string
        Directory to serve
  -CacheSize int
        Number of thumbnails to cache in memory (default 100)
  -Listen string
        Address:port (default "127.0.0.1:8090")

```


## Changelog

2017-06-03 1.0
- Reduce opacity for hidden files
- Improve build system
- [⬇️ webdir-1.0-src.zip](dist-archive/webdir-1.0-src.zip) *(104.16 KiB)*
- [⬇️ webdir-1.0-linux64.tar.gz](dist-archive/webdir-1.0-linux64.tar.gz) *(2.93 MiB)*


2016-10-13
- Initial development release
